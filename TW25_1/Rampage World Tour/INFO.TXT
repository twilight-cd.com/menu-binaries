Rampage World Tour (c) Midway

                 The 90's remake of the 80's hit game Rampage.		      
									     
Features:								     
 									     	
Up to three players go on a killer smash-fest to inflict as much damage                                             destruction as possible
and destruction as possible						     
									     
Demolish buildings, swat down aircraft, eat people and rack up points,     
while destroying entire cities					     
									     
Up to 130 standard levels, 14 bonus levels, 4 grudge match levels and many 
hidden levels 							     
								     
Special bonuses and tasty, nourishing humans give a nice health boost to   
Lizzy, George or Ralph						     
								     
Many secret moves help fend off the constant stream of bullets, fire and   
explosives from the residents, army and the police force                   
								     
Numerous hidden surprises... like Hovercraft, Cheeseheads, and a Used      
Elephant Lot								     

Install Notes

Run SETUP.EXE to                                                            
do a custom install with everything, then REBOOT and play                  

