           Sierra Sports Fantasy Sports Pro (c) Sierra Sports/FSPI          


   Oper. System : Windows 95/98 / DOS   
   Game Type    : Fantasy Sports       
   Req          : P60, 75M HD, Modem, 16M RAM      


                           oOo Release Notes oOo                            
                               ^^^^^^^^^^^^^                                
  Welcome to Fantasy Sports Pro - an exciting, interactive way to watch your 
  favorite sports.  This CD contains all four fantasy sports programs - Terry
  Bradshaw Fantasy Football, Mike Richter Fantasy Hockey, Grant Hill Fantasy 
  Basketball, and Cal Ripken, Jr. Fantasy Baseball.                          
  NOTE: These are all updated with 1998 stats, just in case you're wondering.
  What you're getting is Terry Bradshaw 98, Mike Richter 98, Grant Hill 98,  
  and Cal Ripken 99.                                                         

                           oOo Install Notes oOo                            
                               ^^^^^^^^^^^^^                                
  Run setup, and select the sport you want to install.
  After you've installed, you run the fantasy program that you chose, 
  it will ask you to connect to the net or dial
  through the modem to get the update rosters.
  If there was a way around it, we'd do it, but there isn't. 
  Just follow the instructions, and enjoy the game,