Presto! Photo Album v1.56 (c) NewSoft America Inc.       

Presto! PhotoAlbum keeps your memories alive in personalized "keepsake"    
photo albums. Simply add your favorite photos using a scanner, digital     
camera, or image capturing device. The Photo Browser previews your photos  
and lets you drag-and-drop them into your album, where you can rotate,     
resize, and color-tune them. Then decorate your albums with frames, clip   
art, background textures, shadows, transparencies, and special effects. You
can also add text and voice captions, select album styles and ring styles, 
and produce slide shows with your choice of background music. Finished     
albums can be saved an HTML for easy web publishing, or shared by e-mail as
a self-executable file.                                                    
