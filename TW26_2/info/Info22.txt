3D-WIN version 3.01 for WINDOWS 3.11, 95/NT  

With 3D-Win version 3.01 you can easily print maps and convert      
coordinates to wanted type. This version has 5 languages, you can   
choose the language right after installation.                       
For more info try http://www.3d-system.fi                           

installation                                                       

Win 3.11 version - run setup from \16\disk1 -directory.  
                   Copy 3d.loc from -directory to                  
                   \3dsystem\program -directory after istallation. 

Win 95/NT version - run setup from \32\disk1 -directory.  
                    Copy 3d.loc from -directory to                  
                    \3dsystem\program -directory after istallation. 
