Realism 3D v1.5 

A real-time, interactive 3D graphics program that allows you to easily     
create 3D environments. The easy to learn, drag and drop interface lets you
experiment with and create high-quality designs without expert know-how.   
With Realism 3D you can create interactive picture galleries with          
photographs you have taken, your own web page or professional looking      
documents for proposals and presentations.                                 

When done installing
copy the 2 extracted files, TSHELL.DLL & UNIFILE.DLL, to your
installation directory, overwritting the existing ones there.            
