Photo>Graphics IIP v1.1 for Windows 9X/NT     

TRUESPECTRA Photo>Graphics IIP allows you to combine photos, text and      
drawings for unsurpassed graphics! With Photo>Graphics you can easily      
create, enhance and combine an unlimited number of digital images, text    
and draw objects using the editing and special effects tools available to  
you. Change the color of objects; place text on photographs; duplicate or  
crop images; enhance contrast and brightness or zoom in to highlight the   
most minute detail on a photograph, collage, business presentation or      
advertising flyer.                                                         
                                                                           
Features:                                                                  
Loads common bitmap formats like BMP, JPEG, GIF, TIFF, TGA, etc.         
Export bitmap formats like BMP, JPEG, GIF, TIFF, TGA, etc.               
Print to any printer                                                     
Scan images using any TWAIN scanner                                      
Cut and Paste, or Drag and Drop from other applications.                 
All editing in full 24-bit colour.                                       
Multi-threaded fully 32-bit fast code for best performance.              
Advanced user's interface, supporting direct manipulation and context    
menus.                                                                   
Designed for WINDOWS 95, not an old Windows 3.x upgrade!                 

Name:     Pola Shekhter                                                    
Serial #: TSPW10-000P5-0PP05                                               
