AUDIOGRABBER V1.30 

AudioGrabber is a beautiful piece of software that grabs   
digital audio from cd's. It copies the audio digitally -   
not through the soundcard which enables you to make        
perfect copies of the originals. It can even perform a     
test to see that the copies really are perfect. It can     
also automatically normalize the music, delete silence     
from the start and/or end of tracks, and send them to      
Fraunhofers acm codec or L3enc for automatic creation of   
MP3's.                                                     

run "agsetup.exe"
run "agkey.exe" to get your Serial Number.               
