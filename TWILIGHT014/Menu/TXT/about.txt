
                                              Welcome!

                          Welcome, once again, to another great release of Twilight!

                          There are still two versions of the Windows menu, a 32-bit version for
                        Windows 95/NT (MENU95.EXE) and a 16-bit version for Windows 3.x (MENU311.EXE).
                        We strongly recommend that Windows 3.x users upgrade to Windows 95 or NT.
                        Windows 95 can be found on TL 7, Windows NT 4.0 Workstation on Twilight 5.
                        In the future we will not continue to support Windows 3.x and any enhancements
                        to the menu (there have been a few) will only be implemented in the 32-bit version.

                          If you want just a quick peek at the list of programs and don't bother about the
                        screenshots, then you can also start the Windows 3.x version (menu311.exe) from
                        Windows 95, as we left the screenshots out of that version.

                          We hope you'll all enjoy this great collection of software!


                                              The Wares

                         To be able to fit as many games as possible on one CD, we have compressed
                       nearly all games; Most of them are already big enough! Fortunately, uncompressing
                       these games is extremely easy from the Twilight menu. Just select the drive where
                       you want it to go and click Install. And the game of your choice is uncompressed,
                       and you're ready to go.

                         As for applications, we try put on as many as possible uncompressed for easy
                       installation. Practically every application has its own setup/installation-utility,
                       Installation can be performed directly from the menu for all those applications
                       that are not compressed. In those cases clicking the Install button will directly
                       start the setup/installation-utility from the menu.

                       Note: to make it easy for you to catalogue, print or whatever a file LIST.TXT
                             is included in the Menu directory. In this file you will find the
                             the games and applications that are on the CD.


                                              Betas?!

                         A beta-version of a game or application is a rare thing on Twilight. However,
                       sometimes we decide to put one on; We keep the following rules in mind deciding
                       whether to do so or not:
                          - The program should have an extreme appeal to a lot of people,
                            i.e. newsgroups are already being created on the Internet;
                          - It's stable (we test it as accurately as possible);
                          - The full version is scheduled for release sometime near the End
                            Of Time.


                                              The \VARIOUS-Directory

                        When compiling this CD, we came across a lot of files which were not suitable
                      for inclusion in the main list. However, we figured that these files might be of
                      interest to many people since these usually include trainers, cracks, solves, docs,
                      utilities and usefull things. We selected the most interesting files and put them
                      together in the \VARIOUS-directory of the CD.  Please note though, that we did NOT
                      check any of these files (except those that are mentioned in the Release Info,
                      games and apps descriptions)! A list of all files in the \VARIOUS-directory with a
                      short description (in most cases just the full name of the program) can be viewed
                      by selecting the various tab in the menu. You can extract the file to a directory
                      of your choice by clicking the install button, after which a directory selectbox
                      will pop-up.

                      The various list contains the names of the programs instead of the zip files
                      for easier browsing, if you need it the name of the ZIP file is displayed in
                      the description box.
                      
                      Note: the various directory also contains a file VARIOUS.LST which you can use
                            for your easy reference, printing or whatever.


                                              The Twilight Installer/Menu

                        The Twilight Installer enables you to easily select and unpack a program.  Some
                      of its features are:
                          - Online Help-Service (you're reading it)
                          - Screenshot-viewing
                          - Simultaneous viewing of the menu and installing
                          - Simultaneous installing of multiple programs
                          - Auto-checking for free diskspace
                          - Paste&Copy serial numbers
                          - View contents of previous releases
             new ==>      - Explore directories and files using your own favorite viewer

                      When you have selected a program you want to install click on the install button.
                      If it is program that is packed it will be extracted to drive you selected using
                      the drive selector which will also indicate if you have enough free space on that
                      drive. When you selected a program that is not packed on the CD then the apropriate
                      setup program will be launched. In some cases you have to extract to a directory
                      that is specific to your system, you then will be prompted with a directory selector.

                        When you browse through the list the screenshots will automatically appear. If a
                      title has more than one screenshot you can view them using the [<<] and [>>] buttons.

                        When you've selected a title and launched it's installation process by clicking the
                      install button, you can continue browsing through the menu and install other titles
                      as you wish.

                        By clicking on the numbers in that are in the upper graphics box you can
                      view the contents of previous releases.

                        Some (mostly windows applications) require you to fill in a serial number during
                      or after installation. If so then the serial number is included in the description
                      of the program. Use you're mouse to select it and then press Ctrl-C to copy it into
                      the clipboard buffer. When prompted for the serial number make sure the cursor is
                      active in the right box and press Ctrl-V to paste the number from the clipboard.
                      This will save you remembering or writing down the serial number.

             new ==>    If you want to take a look at the contents of a directory or zipfile first
                      before installing. Or if you need to copy some crack or something directly
                      from the CD you can press the Explore button. In case the selected program
                      resides in its own directory an explorer window containing that directory will
                      be opned. If the program resides in a ZIP file the program that YOU have
                      associated with zipfiles will be started (eg. WinZip).


                                              Not Enough Free Diskspace?

                        The installer will automatically check if there's enough free diskspace on the
                      selected harddisk. If a program that is unpacked needs additional installation then
                      the required size that is indicated is that of the -intermediate- files, not the
                      final installation, so you will always have enough space to unpack (before this was
                      the other way around). Games that use an intermediate installation will in some cases
                      need about the same amount for final installation and the intermediate one, in other
                      cases the intermediate installation will still be used as the "Original" CD.
                      Applications that are installed using their custom installation-program quite often
                      offer a variety of different installation-options the size that is indicated is the
                      size that is required for a Typical, Default or common installation. For an application
                      that is stored in compressed format on the CD the size of the decompressed/intermediate
                      installation is indacated (same as with the games).


                                              Twilight Easy Installation

                        We included a small program called TL.EXE with most games that run in a DOS mode.
                      It enables you to select common operations, such as Setup or Run Game from a menu.
                      Make your choice using the cursor-keys and push Enter to select. Also available with
                      some titles is Read Documentation; We included a small doc-file with some, giving you
                      information on how to install, trouble shooting or anything else that might be of
                      importance.  So make sure you take a look at it! In some cases a TL.BAT is provided
                      instead of a TL.EXE so just type TL and you'll be OK. Please note that TL.EXE is never
                      included with a Windows-only game. As the menu is very easy to access and you might
                      want to know in advance we mostly provide information about special install etc. in
                      the description of the program in the menu, PLEASE DO READ IT!


                                              Software Testing

                        We tested ALL programs on a couple of different systems with totally different
                      configurations (CPU, Memory, Network, Harddisk, OS, etc.). We have found all programs
                      to work perfectly, but it is possible that you run into trouble.  If you do, please
                      read the Troubleshooting-section later on.  There are millions of different PCs out
                      there...  Some people told us that they couldn't get certain games to work. Once again
                      we say that all games work on our machines. So if a game doesn't run, first read any
                      the complete description of the program in the menu (you might have overlooked some
                      special install instructions) next check for a DOCS&NFO\TL.TXT to see if there are hints
                      for installing and running a program. If the Troubleshooting-section didn't help either,
                      then it's probably not our fault but the programmers of the game's problem! Some people
                      are still forgetting to read all the information we provide, please read it before you
                      decide a game doesn't run.


                                              Operating System indication

                        With each game or application the OS it runs on is given. Below an explanation
                      of the types used (they are pretty self explanatory though):

                            - DOS, this is a native DOS program. It is possible that it will run in a DOS
                                   box under Windows on your system. What it says is that we encountered
                                   problems running this program under Windows with at least one of the
                                   machines we tested on.
                            - Win95, this is a Windows program that will run in Windows 95, don't bother
                                   trying this if you're still using Windows 3.x.
                                   Though not indicated this program will most likely run in Windows NT,
                                   this is however not tested.
                            - DOS/Win95, this is either a dual program, ie two versions a supplied one for
                                   each OS or it is a DOS based program that will run in a Windows DOS box.
                            - Win95/NT, will run on both, 95 and NT.
                            - Windows, program that will run on Windows 3.x and Windows 95.
                            - Win311, nah, you won't find any of these on Twilight.


                                              Virus Checking

                        We have performed virus-checking with the latest versions of:

                            - Thunderbyte Anti-Virus (TBAV);
                            - McAfee Scan;
                            - F-Prot;
                            - Dr Solomon's Toolkit.

                        This CD is 100% free of any known viruses; It's absolutely impossible for any known
                      virus to make it through to the final version of the CD! As stated above, we scan with
                      the latest versions of the best virus-scanners; Only if all scanners are unable to
                      detect a certain virus, we're defenseless.  Since we also use heuristic scanning, that's
                      not very likely to happen.


                                              Got A Problem?!

                        This section is intended as a trouble-shooting guide; It tries to give you some
                        advice if you cannot get a program to work.

                           - Your joystick can't be calibrated. A lot of windows games don't have the
                             of calibrating your joystick. This has to be done from the Windows control
                             panel. Select Setting->Control Panel from the Windows Start menu and double
                             click Joystick and then calibrate it.
                           - The game you're trying to run needs DirectX, it's on the CD install it.
                           - Some games require "Z" technology, ie your Z drive has to be
                             available for SUBSTing. Put LASTDRIVE=Z in your CONFIG.SYS to fix this.
                             Formerly we would use the A drive for substing but this disables access
                             to your floppy drive, so we changed it. Also make sure SUBST.EXE is in
                             your path (eg. your DOS-directory).
                           - If you're immediately getting a PKUNZIP returned an error while
                             depacking error when trying to unZIP a program, you're probably
                             low on memory.  Try freeing up some more conventional memory
                             (memory below the 640K border) before trying again.
                           - Trying to run a program which requires 16 MB of memory when you've
                             only got 8 doesn't make sense.
                           - If you're low on diskspace, try removing some files.  Some
                             programs (especially under Windows!) require some free diskspace
                             to fiddle around with.
                           - Try increasing the FILES= statement in your CONFIG.SYS.
                           - Remove SMARTDrive (when running DOS mode).
                           - If you're using Windows, put the Windows-directory in your path.
                           - Try using many different startup-files (CONFIG.SYS/AUTOEXEC.BAT)!
                             Here are some suggestions:
                               - Change memory-managers.  Change QEMM to EMM386 and vice versa.
                                 (or better yet, remove them! as modern software will easily
                                 run without them)
                               - Use only HIMEM.SYS.
                               - Try using no startup-files at all (clean boot).  To do this,
                                 push Left-Shift at bootup.
                               - Remove all TSRs from your startup-files (or at least the ones
                                 you don't really need).  Quite often they cause a conflict.
                           - If a network is not required, disable it.
                           - Remember to setup your soundcard properly!  If you get it wrong,
                             it might just crash your machine.  Many sound-setup programs offer
                             some sort of auto-detection; However, if this process crashes your
                             machine you have to set the parameters manually.
                           - Some DOS games moan something about VESA 2.0 or a linear-frame buffer
                             if your video card is not up to it try installing a software VESA
                             driver, like Scitech Display Doctor.
                           - If a Windows game won't run try using a diffent resolution/color depth
                             numerous Windows games insist on having a 256 color display,
                             no more, no less.
                           - Get the latest drivers for your hardware, especially your
                             video and audio card. Mostly found on their manufacturors Web site.
                           - Make sure your DirectX is working properly.
                           - If some cracked game complains about needing the CD, check if there
                             is a CD-Rom in your CD drive, if so remove it and try again. If not
                             put one in and try again!


                                              Twilight Copyleft

                        Be aware of cheap imitations!  Only the real Twilight brings you the
                      real quality you're used to get from us, and at the price you payed for
                      all these wonderful programs, it's an absolute givaway.


                                              Thanks N' Greets

                        We hope you enjoyed this release of Twilight as much as the Dalmatians did!

                        Till next time on our double adventure!

   
                           Until then: Woof woof

                           Signed:  The Twilight Crew

