# Run with Python 3.6+, developed on macos. Will only work on mac os.
# call with: python3 extract.py
import os
import shutil
import subprocess
import time
from copy import copy
from os import listdir
import logging
import re

# setup some logging facility
log = logging.getLogger(__package__)
log.setLevel(logging.DEBUG)

ch = logging.StreamHandler()
ch.setLevel(logging.DEBUG)
ch.setFormatter(logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s'))
log.addHandler(ch)


OUTPUT_DIRECTORY = "../"
MOUNT_DIRECTORY = "/volumes"
MAXIMUM_FILE_SIZE = 1024 * 1024 * 5  # 5 megabyte in bytes, the menu is about 3.5 megabytes



def add_git_ignore_files_to_empty_directories():
    """
    Keep empty games/apps directories, add those to gitlab (which will add a gitignore to their contents and changes
    their mtime. But that's better than having NO content to github. The mtime should be added to the gitignore file.

    :return:
    """

    git_ignore_contents = """# Ignore everything in this directory
*
# Except this file
!.gitignore
#
# Original mtime of this directory: {mtime}
    """

    pass


def get_mounted_twilights():
    # Get all current mounts
    volumes = listdir(MOUNT_DIRECTORY)

    # it's possible to have multiple discs mounted, as i've multiple readers.
    twilights = []
    # Twilight in the 50's range are prefixed with TWILIGHT
    twilights += [f for f in volumes if "TWILIGHT" in f]

    # Twilight in the 36's range are prefixed with Twilight
    twilights += [f for f in volumes if "Twilight" in f]

    # DVD 51 and higher
    twilights += [f for f in volumes if "tl" in f]

    # twilights in the 80's range are called 82B (the DVD is)
    twilights += [f for f in volumes if matches_later_release(f)]

    # Fake Twilights
    twilights += [f for f in volumes if f in ["TW25_1", "TW25_2", "TW26_1", "TW26_2"]]

    # Dubious Twilights, DVD 50.
    twilights += [f for f in volumes if f in ["DISC50", "DISC59"]]

    # TL Two:
    twilights += [f for f in volumes if f in ["TL2_1_01", "TL2_1_02", "TL2_2_01", "TL2_2_02"]]

    # DVD65 is undefined :)
    twilights += [f for f in volumes if f in ["UNDEFINED"]]

    return twilights


def matches_later_release(mountpointname):
    #82B, 89A
    return re.findall("^[0-9]{2,3}[AB]{1}$", mountpointname)


def import_twilight(release: str = "TWILIGHT59A"):
    """
    Requirements:
    - Copies file including ctime and mtime.
    - Does not copy large / copyrighted files
    - Can handle any twilight release layout
    - Files and release names are litterally the names of the disc: including high/lowercase mixes etc.

    Listing from #59, probably 010-125...
    Autorun.inf
    Games
        games.001
    Menu
        List.txt
        Pics
            *.001
        TL.ICO
        TL59desktop.jpg
        UNRAR.DLL
    TL.ICO
    menu95.exe

    :param release:
    :return:
    """

    # if the release is a DVD, add [DVD] to the directory name. Because CD number 2 and the DVD have the same
    # filename. EG: TWILIGHT48B is used for the CD and DVD.
    if is_dvd(f"{MOUNT_DIRECTORY}/{release}"):
        output_release = f"{OUTPUT_DIRECTORY}/{release} [DVD]"
    else:
        output_release = f"{OUTPUT_DIRECTORY}/{release}"

    # shield the copytree from entering the copy routine and finding out the dir already exists.
    if os.path.isdir(output_release):
        print(f"Waiting for a twilight to be inserted...  Already imported {release}.", end='\r')
        return

    log.info(f"Going to import {release}.")

    try:
        # fun: it sometimes copies permissions of the disc and i'm stuck with a dir i can't enter :)
        shutil.copytree(f"{MOUNT_DIRECTORY}/{release}", output_release, ignore=do_not_copy_this)
    except FileExistsError:
        log.warning(f"{release} already imported.")
        return

    log.info(f"Import {release} finished.")


def is_dvd(path_to_volume):
    # https://stackoverflow.com/questions/45079033/python-get-file-size-of-volumes-or-physical-drives
    statvfs = os.statvfs(path_to_volume)
    filesystem_size = statvfs.f_frsize * statvfs.f_blocks  # Size of filesystem in bytes

    # dvd's > 750 megabyte. And because there are only cd's and dvd's its good enough to just check if something
    # is larger than 750 meg :)
    cd_size = 1024 * 1024 * 750
    if filesystem_size > cd_size:
        return True
    return False


def do_not_copy_this(current_path_on_disk, content_in_directory):
    """
    This helps filtering what files should be copied. The directories such as GAMES should be copied,
    and should be empty. They also should be uploaded to gitlab, even while they are empty.

    :param current_path_on_disk: the current path we're on, on the disk.
    :param content_in_directory os.listdir() content.
    :return:
    """
    ignored_files = []

    # log.debug(f"Entering directory: {current_path_on_disk} containing {len(content_in_directory)} files.")

    current_directory_name = current_path_on_disk.rsplit('/', 1)[-1]

    # ignore all the directories in the Apps folder, as they are copyrighted and the directory structure is not OC
    # this will perhaps pickup some text files which are oc.
    # Twilights 0-10 had APPS and GAMES
    if current_directory_name in ["Apps", "Games", "APPS", "GAMES"]:
        ignored_files += [file for file in content_in_directory if os.path.isdir(f"{current_path_on_disk}/{file}")]

    # the various directory contains very small files, but ZIP files should be ignored
    # Twilgihts 0-10 had VARIOUS
    if current_directory_name in ["Various", "VARIOUS"]:
        ignored_files += [file for file in content_in_directory if file_extention(file) in [".zip", ".exe", ".rar"]]

    # ignore exe in apps:
    if current_directory_name in ["Apps", "APPS"]:
        ignored_files += [file for file in content_in_directory if file_extention(file) in [".exe", ".zip", ".rar"]]

    # ignore rar files in games:
    if current_directory_name in ["Games", "GAMES"]:
        ignored_files += [file for file in content_in_directory if file_extention(file) in [".rar", ".exe", ".zip"]]

    # some custom stuff on the very old releases
    if current_directory_name in ["THEMES", "QUAKE", "MR_BIOS", "REDALERT", "TLESSENT"]:
        ignored_files += [file for file in content_in_directory if file_extention(file) in [".zip", ".A01", ".ARJ",
                                                                                            ".A02", ".EXE"]]

    # ignore all larger files, so games.001 is not copied.
    ignored_files += [file for file in content_in_directory
                      if os.path.getsize(f"{current_path_on_disk}/{file}") > MAXIMUM_FILE_SIZE]

    # some exceptions: the wallpapers from the various map CAN be copied. These are the following files:
    # ... tbd.

    # todo: should i still add the empty dirs that are removed here?
    # these do not adhere to all patterns above, and have to be excluded manually:
    exclusions = [
        {"disc": "TWILIGHT001", "path": "DIG.EXE", "reason": "Copyrighted game"},
        {"disc": "TWILIGHT001", "path": "DIG", "reason": "Copyrighted game."},
        {"disc": "TWILIGHT003", "path": "VARIOUS/TNV109.TXT", "reason": "Part of copyrighted content"},
        {"disc": "TWILIGHT007", "path": "BUBBLE/BB.EXE", "reason": "Part of copyrighted content"},
        {"disc": "TWILIGHT008", "path": "INSTALL", "reason": "Part of copyrighted content"},
        {"disc": "TWILIGHT008", "path": "INSTALL.BAT", "reason": "Part of copyrighted content"},
        {"disc": "TWILIGHT18B", "path": "WIN98", "reason": "Complete Windows 98 release, copyrighted"},
        {"disc": "TWILIGHT22A", "path": "Various/WinG", "reason": "Copyrighted software"},
        {"disc": "TWILIGHT23A", "path": "Various/WinG", "reason": "Copyrighted software"},
        {"disc": "TWILIGHT24B", "path": "win98", "reason": "Final release of windows 98."},
        {"disc": "TWILIGHT26B", "path": "WIN98", "reason": "Another release of windows 98."},
        {"disc": "TWILIGHT32B", "path": "Various/WinG", "reason": "Copyrighted software"},

        # fake stuff
        {"disc": "DISC50", "path": "Menu/A", "reason": "Different location for apps"},
        {"disc": "DISC50", "path": "Menu/G", "reason": "Different location for games"},
        {"disc": "DISC50", "path": "Menu/V", "reason": "Different location for various"},
        {"disc": "DISC59", "path": "MENU/A", "reason": "Different location for apps"},
        {"disc": "DISC59", "path": "MENU/G", "reason": "Different location for games"},
        {"disc": "DISC59", "path": "MENU/V", "reason": "Different location for various"},
        {"disc": "TL2_1_01", "path": "rar", "reason": "Copyrighted content"},
        {"disc": "TL2_1_02", "path": "rar", "reason": "Copyrighted content"},
        {"disc": "TL2_2_01", "path": "mp3/music.mp3", "reason": "Copyrighted content"},
        {"disc": "TL2_2_01", "path": "rar", "reason": "Copyrighted content"},
        {"disc": "TL2_2_02", "path": "mp3/music.MP3", "reason": "Copyrighted content"},
        {"disc": "TL2_2_02", "path": "rar", "reason": "Copyrighted content"},
        {"disc": "85B", "path": "Misc", "reason": "Keygens etc"},
        {"disc": "84B", "path": "Misc", "reason": "Keygens etc"},
    ]

    # calculate paths for exclusions, based on disk, current mount location and excluded path
    excluded_paths = []
    for exclusion in exclusions:
        excluded_paths.append(f"{MOUNT_DIRECTORY}/{exclusion['disc']}/{exclusion['path']}")

    ignored_files += [file for file in content_in_directory if f"{current_path_on_disk}/{file}" in excluded_paths]

    # fakes and other weird stuff
    ignored_files += handle_fake_twilights(current_path_on_disk, content_in_directory)

    # has not been needed yet.
    # the ignored files do not contain a path, so there might be a small risk of path clashes.
    # otoh: probably will never happen.
    inclusions = [  # noqa
        {"disc": "TWILIGHT009", "path": "VARIOUS/TP6", "reason": "Demos from The Party 6, freely available online."},
        {"disc": "TWILIGHT009", "path": "VARIOUS/TP6_64KB", "reason": "Demos from The Party 6, available online."},
    ]

    copied_files = len(content_in_directory) - len(set(ignored_files))
    log.debug(f"In: {current_path_on_disk}, copying {copied_files}/{len(content_in_directory)} files.")

    return ignored_files


def handle_fake_twilights(current_path_on_disk, content_in_directory):
    """
    The file structure of fake twilights is radically different. It's ugly!

    :param current_path_on_disk:
    :param content_in_directory:
    :return:
    """

    ignored_files = []

    exclusions = [
        {"disc": "TW25_1", "path": "House of the Dead/THOTD.EXE", "reason": "Copyrighted game"},
        {"disc": "TW25_1", "path": "House of the Dead/TRAINER.EXE", "reason": "Copyrighted game"},
        {"disc": "TW25_1", "path": "House of the Dead/TRAINER.NFO", "reason": "Copyrighted game"},
        {"disc": "TW25_1", "path": "icarus/icarus.exe", "reason": "Copyrighted game"},
        {"disc": "TW25_1", "path": "icarus/TRAINER.EXE", "reason": "Copyrighted game"},
        {"disc": "TW25_1", "path": "Interactive Golf 99/foxgolf99.exe", "reason": "Copyrighted game"},
        {"disc": "TW25_1", "path": "MECH COMMANDER/MECHCOMM.EXE", "reason": "Copyrighted game"},
        {"disc": "TW25_1", "path": "Pinball Soccer '98 (c) Pinball Games Ltd/PINBALL.exe", "reason": "Copyrighted game"},
        {"disc": "TW25_1", "path": "Rampage World Tour/RAMPAGE.exe", "reason": "Copyrighted game"},

        {"disc": "TW25_2", "path": "Rampage World Tour/RAMPAGE.exe", "reason": "Copyrighted game"},
    ]

    excluded_paths = []
    for exclusion in exclusions:
        excluded_paths.append(f"{MOUNT_DIRECTORY}/{exclusion['disc']}/{exclusion['path']}")

    ignored_files += [file for file in content_in_directory if f"{current_path_on_disk}/{file}" in excluded_paths]

    # TW25_2 has a lot of files, we're going to whitelist instead of blacklist: So first remove everything!
    if "TW25_2" in current_path_on_disk:
        ignored_files += content_in_directory

        whitelisted = [
            # normal files
            "AutoRun.inf", "Twilight.ini", "tw.bmp", "TW25.txt", "Twilight.exe", "INFO.TXT", "FILE_ID.DIZ", "INFO2",
            "RECYCLED", "DD0.TXT", "DD1.EXE", "DD2.INI", "DD3.DAT", "DD4.LOG", "desktop.ini",

            # and all directory names to get the info files from:
            "Web Weasel v3.0 Deluxe", "Vimas Speech Master Studio v3.0", "VuePrint Pro 32-bit 6.0e", "Web FerretPro 2.6102",
            "System Commander v2.5", "Awave v4.5", "BackGround Master v1.0", "BORLAND JBUiLDER 4 ADDON TOOLS",
            "BORLAND JBUiLDER 4oo", "Cakewalk Professional 7.0", "CDH Media Wizard v2.6", "Delphi v4 Client Serve",
            "DIGITAL DARKROOM v1.2", "Download Butler v2.13", "Dragon Point & Speak v3.0", "Easy Talk 2.1",
            "Enhanced Notepad v1.0", "Erazer 98 v2.11", "File Shrreder v2.2", "Fractal Design Poser v3.0", "FTPPro98 v6.20",
            "Ghost 5.0e", "Gif Constuction Set 1.0Q", "HyperSnap-DX v3.13 Basic", "HyperSnap-DX v3.13 Pro",
            "IOMEGA ZIP TOOLS V5.4", "LapLink Tecnical v1.5", "Lorenz Graf`s HTMLtool v2.01", "Making Waves v1.8",
            "MICROSTATION SE V5.07.00.41", "MIRC V5.4", "NetIntellect v3.02", "NEWSBOT V5.9", "Norton Utilities 3.07 win98",
            "Nuts and Bolts 98", "Paint Shop Pro 5.01", "PHOTOSPRAY V1.0 FOR ADOBE PHOTOSHOP",
            "Picture Taker Enterprise Edition v1.5", "PowerBar v1.65", "Repligator v3.5", "SKYPAINT V1.04", "SnagIt v4.2.1",
            "SQUIZZ V3.04", "Super Text Search 1.5"
        ]

        # Do not alter the ignored files object while iterating over this, the results are unpredictable:
        original_ignored_files = copy(ignored_files)
        for ignored_file in original_ignored_files:
            if ignored_file in whitelisted:
                ignored_files.remove(ignored_file)

    if "TW26_2" in current_path_on_disk:
        ignored_files += content_in_directory

        whitelisted = [
            # Retrieve all info files.
            "Twilight.ini", "3-D Fotocube Version 1.41", "3D-WIN version 3.01", "Acccount Pro v7.0a", "Ace Ftp 1.03",
            "Ace Replace V1.0", "AudioGrabber V1.30", "AutoRun.inf", "BluePrint v1.0.50", "Cleansweep Admin v4.02",
            "CuteFX v1.0", "directx", "eSafe Protect Virus scanner", "FaxNow! v3.1", "FTPEdit v2.10", "goScreen v1.0",
            "Greeting Card Generator", "Hot Backgrounds 2.0", "Hot Buttons 2.0",
            "HotDog Professional Web Master Suite v5.0", "info", "list.txt", "Lotus Notes v4.6a",
            "Macromedia Dreamweaver V1.2a", "MailAlert.v2.02.19", "MsCode v6.3", "Music Ref 4.0", "My Phonebook v3.34",
            "Namo WebEditor v2.03", "NewsGrabber v2.1.10", "OSCAR 10.0", "OSCAR 10.1 update", "PACT Ghosts v98.8",
            "Partition-It Extra Strength v1.11", "Phone Plus v2.4", "Photo Graphics IIP v1.1", "PhotoModeler Pro v3.0",
            "PhotoRecall Deluxe V2.0", "Power Clock 3.04", "Presto! Photo Album v1.56",
            "Psychedelic ScrSavers v3.9.971", "Realism 3D v1.5", "Seagate Backup Exec Desktop 98", "SmartDraw v3.24",
            "Super Mail 2.2p", "Symantec Norton Your Eyes Only v4.1", "Tabnotes v3.06", "TweakDUN v2.11.117",
            "Twilight.exe", "Ulead FX RAZOR 1.0", "Ulead MediaStudio Pro v5.0", "VuePrint v6.1 Pro",
            "WillowTALK v. 2.01.001", "win98",
            "Info1.txt", "Info2.txt", "Info3.txt", "Info4.txt", "Info5.txt", "Info6.txt", "Info7.txt", "Info8.txt", "Info9.txt",
            "Info10.txt", "Info11.txt", "Info12.txt", "Info13.txt", "Info14.txt", "Info15.txt", "Info16.txt", "Info17.txt", "Info18.txt", "Info19.txt",
            "Info20.txt", "Info21.txt", "Info22.txt", "Info23.txt", "Info24.txt", "Info25.txt", "Info26.txt", "Info27.txt", "Info28.txt", "Info29.txt",
            "Info30.txt", "Info31.txt", "Info32.txt", "Info33.txt", "Info34.txt", "Info35.txt", "Info36.txt", "Info37.txt", "Info38.txt", "Info39.txt",
            "Info40.txt", "Info41.txt", "Info42.txt", "Info43.txt", "Info44.txt", "Info45.txt", "Info46.txt", "Info47.txt", "Info48.txt", "Info49.txt",
            "Info50.txt", "Info51.txt", "Info52.txt", "Info53.txt", "Info54.txt", "Info55.txt", "Info56.txt", "Info57.txt", "Info58.txt", "Info59.txt",
            "Info60.txt", "Info61.txt", "Info62.txt", "Info63.txt", "Info64.txt", "Info65.txt", "Info66.txt", "Info67.txt", "Info68.txt", "Info69.txt",
        ]

        # Do not alter the ignored files object while iterating over this, the results are unpredictable:
        original_ignored_files = copy(ignored_files)
        for ignored_file in original_ignored_files:
            if ignored_file in whitelisted:
                ignored_files.remove(ignored_file)

    return ignored_files


def file_extention(filename: str) -> str:
    filename, extension = os.path.splitext(filename)
    # there are often case mixes such as .ZIP and .zip. Normalize them:
    return extension.lower()


def eject_cd():
    process = subprocess.Popen("drutil eject".split(), stdout=subprocess.PIPE)


def import_routine():
    for twilight_release in get_mounted_twilights():
        try:
            import_twilight(twilight_release)
        except PermissionError:
            """
            Traceback (most recent call last):
              File "extract.py", line 127, in <module>
                import_twilight(twilight_release)
              File "extract.py", line 60, in import_twilight
                shutil.copytree(f"{MOUNT_DIRECTORY}/{release}/", f"{OUTPUT_DIRECTORY}/{release}/", ignore=do_not
              File "/Library/Frameworks/Python.framework/Versions/3.6/lib/python3.6/shutil.py", line 309, in cop
                names = os.listdir(src)
            PermissionError: [Errno 13] Permission denied: '/volumes/TWILIGHT59B/'
            """
            log.error(f"{twilight_release}: permission error. Disc is probably not mounted yet.")
        except OSError:
            """
            Traceback (most recent call last):
              File "extract.py", line 127, in <module>
                import_twilight(twilight_release)
              File "extract.py", line 60, in import_twilight
                shutil.copytree(f"{MOUNT_DIRECTORY}/{release}/", f"{OUTPUT_DIRECTORY}/{release}/", ignore=do_not
              File "/Library/Frameworks/Python.framework/Versions/3.6/lib/python3.6/shutil.py", line 309, in cop
                names = os.listdir(src)
            OSError: [Errno 5] Input/output error: '/volumes/TWILIGHT59B/'
            """
            log.error(f"{twilight_release}: permission error. Disc is probably not mounted yet.")

        # can we eject what has been done? Then we can loop this script :)
        eject_cd()


if __name__ == "__main__":
    log.info("Going to import Twilight CDs/DVDs that are mounted.")

    try:
        while True:
            # Print a status message that doesn't clog up the command line while waiting.
            print("Waiting for a twilight to be inserted...                                                 ", end="\r")

            import_routine()
            time.sleep(3)
    except KeyboardInterrupt:
        log.info("Stopped :)")
