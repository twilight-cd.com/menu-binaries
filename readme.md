            -�����������������������������������������������������������������������������
                          ��������             ���              ��
                         ��  ��  ��             ��              ��     ��
                             ��            ��   ��   ��         ��     ����� �� �  �   �
                            ���   ��   �� ���   ��  ���  ������ ������ ��
                            ���   �� ���� ���  ���  ���  ��  �� �� ��� ���
              �   �  �� ��������  ������� ���� ���� ���� ������ �� ��� �����
                                                            ���
                                                          ����
            ����������������������������������������������������������������������������-�

                                  ___.   .__                                 .___
      _____   ____   ____  __ __  \_ |__ |__| ____ _____ _______ ___.__.   __| _/_ __  _____ ______
     /     \_/ __ \ /    \|  |  \  | __ \|  |/    \\__  \\_  __ <   |  |  / __ |  |  \/     \\____ \
    |  Y Y  \  ___/|   |  \  |  /  | \_\ \  |   |  \/ __ \|  | \/\___  | / /_/ |  |  /  Y Y  \  |_> >
    |__|_|  /\___  >___|  /____/   |___  /__|___|  (____  /__|   / ____| \____ |____/|__|_|  /   __/
          \/     \/     \/             \/        \/     \/       \/           \/           \/|__|

                                      == Menu Binary Dump ==

# Introduction

This is a dump of all binary and original content by the Twilight Crew in the 90's. All content was taken from CD 1 to
75, DVD 48 to 89 and a number of fakes.

# Todo

    ====    =======     ===============
    Done    Release     Action
    ----    -------     --------------
            25 B        Replace disc because of read errors
            TW26_1      Replace (bootleg) disk because of read errors
            31 B        Spinning errors: cannot be read completely, cannot read various.list
            37 B        Spinning errors: cannot be read completely, cannot read various.list
            071DVD      does not mount
            072DVD      does not mount
            073DVD      does not mount
            074DVD      does not mount
            075DVD      does not mount
            076DVD      does not mount
            077DVD      does not mount
            078DVD      does not mount
            079DVD      does not mount


# Creation

All content is consistently ripped by the extractor tool that is included in this repository. This ensures data is
copied from disc in a consistent manner. Copied information contains original creation dates. Modification times are
slightly altered because of MacOS willingness to write invisible files everywhere.


# How to run the import tool

The included extract.py script runs with >=python3.6 and defaults to MacOS defaults of having volumes mounted in
/volumes. No effort was taken to make this tool even remotely portable beyond macos.

```
cd "_ extractor"
python3 extract.py
```


# No Piracy

Do not ask for twilight images. This repository only contains original content created by the (many) Twilight crews.


# Background

More background on these files can be found at https://twilight-cd.com.
